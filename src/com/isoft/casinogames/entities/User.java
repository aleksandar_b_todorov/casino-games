package com.isoft.casinogames.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class User
{
	private String nickname;
	private String email;
	private String password;
	private String firstName;
	private String middleName;
	private String lastName;
	private int age;
	private String passportNumber;
	private BigDecimal money;
	private boolean isAdmin;
	private boolean isBlocked;
	private List<String> fundsHistory;
	
	public User(String nickname, String email, String password, String firstName, String middleName, String lastName,
				int age, String passportNumber, BigDecimal money, boolean isAdmin, boolean isBlocked)
	{
		this.nickname = nickname;
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.age = age;
		this.passportNumber = passportNumber;
		this.money = money;
		this.isAdmin = isAdmin;
		this.isBlocked = isBlocked;
		this.fundsHistory = new ArrayList<>();
	}

	public User() {}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public String getPassportNumber()
	{
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber)
	{
		this.passportNumber = passportNumber;
	}

	public BigDecimal getMoney()
	{
		return money;
	}

	public void setMoney(BigDecimal money)
	{
		this.money = money;
	}

	public boolean isAdmin()
	{
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin)
	{
		this.isAdmin = isAdmin;
	}

	public boolean isBlocked()
	{
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked)
	{
		this.isBlocked = isBlocked;
	}

	public List<String> getFundsHistory()
	{
		return fundsHistory;
	}

	public void setFundsHistory(List<String> fundsHistory)
	{
		this.fundsHistory = fundsHistory;
	}

	@Override
	public String toString()
	{
		return "Username: " + nickname + ", Email: " + email + ", Name: " + firstName + " "
		+ middleName + " " + lastName + ", Age: " + age + ", Passport number: "
				+ passportNumber + ", Money: " + money + ", Аdmin: " + isAdmin + ", Blocked: " + isBlocked;
	}
}
