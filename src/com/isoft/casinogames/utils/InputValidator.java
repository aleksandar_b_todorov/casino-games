package com.isoft.casinogames.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class InputValidator
{
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._-]+@[A-Z0-9._-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);
    private static final String ONLY_LETTERS_DIGITS_UNDERSCORE_REGEX = "\\w+";
    private static final String LETTERS_HYPHEN_REGEX = "[A-Za-z-]+";
    private static final String LETTERS_DIGITS_3_20_REGEX = "^(?!^0+$)[a-zA-Z0-9]{6,20}$";
    private static final String MONEY_REGEX = "^[0-9]+(\\.[0-9]{1,2})?$";

    private InputValidator(){}

    public static boolean validateEmail(String emailStr)
    {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public static boolean validateLettersAndHyphen(String Str)
    {
        return Str.matches(LETTERS_HYPHEN_REGEX);
    }

    public static boolean validateOnlyLettersDigitsMin6Max20(String Str)
    {
        return Str.matches(LETTERS_DIGITS_3_20_REGEX);
    }

    public static boolean validateLettersDigitsUnderscore(String Str)
    {
        return Str.matches(ONLY_LETTERS_DIGITS_UNDERSCORE_REGEX);
    }

    public static boolean validateMoney(String Str)
    {
        return Str.matches(MONEY_REGEX);
    }

    public static boolean validateLength(String str, int min, int max)
    {
        return str.length() > min && str.length() < max;
    }

    public static boolean validateAge(int age)
    {
        return age < 18 || age > 90;
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    public static boolean validateRouletteNumber(String str)
    {
        if (isNumeric(str))
        {
            return Integer.parseInt(str) >= 0 && Integer.parseInt(str) <= 36;
        }

        return false;
    }
}
