package com.isoft.casinogames.utils;

import com.google.gson.reflect.TypeToken;
import com.isoft.casinogames.entities.User;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

public final class GlobalConstrains
{
    private GlobalConstrains(){}

    public static final String WELCOME_APP_MESSAGE = "===================\nWelcome to Casino Games App\n===================";
    public static final String FUNC_DOESNT_WORK_TRY_AGAIN = "This functionality doesn't work right now. Please try again later.";
    public static final String IOSTREAM_FILE_BROKEN = "IOStream for %s were broken. - ";
    public static final String FILE_NOT_FOUND = "%s could not be found. - ";
    public static final String JSON_PATH_MESSAGES = "./src/resources/messages.json";
    public static final String JSON_PATH_USERS = "./src/resources/users.json";
    public static final String FILE_PATH_LOG4J_PROPERTIES = "./src/resources/log4j.properties";


    public static final Type HASHMAP_TYPE = new TypeToken<HashMap<String, String>>(){}.getType();
    public static final Type LIST_USER_TYPE = new TypeToken<List<User>>(){}.getType();
}
