package com.isoft.casinogames.utils;

import com.isoft.casinogames.streamio.impl.JsonStreamIO;

public final class UserUniqueValidator
{
	private static JsonStreamIO jsonStreamIO = new JsonStreamIO();

	private UserUniqueValidator(){}
	/*
	 * @return true if there is no such key:value pair in the file
	 * @return false if there is already User with such key:value pair
	 */
	public static boolean isEmailUnique(String email)
	{
		return jsonStreamIO.readByEmail(email) == null;
	}

	public static boolean isNicknameUnique(String nickname)
	{
		return jsonStreamIO.readByNickname(nickname) == null;
	}
}
