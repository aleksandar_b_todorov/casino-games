package com.isoft.casinogames.utils;

import com.isoft.casinogames.streamio.impl.JsonStreamIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/*
 * Implementing PBKDF2 Hashing algorithm
 */
public final class PasswordEncrypter
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonStreamIO.class);

    private PasswordEncrypter(){}

    public static String encrypt(String password)
    {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), new byte[1], 65536, 128);
        SecretKeyFactory factory = null;
        try
        {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        } catch (NoSuchAlgorithmException nsae)
        {
            LOGGER.error("Encryption error. - " , nsae);
            System.out.println(GlobalConstrains.FUNC_DOESNT_WORK_TRY_AGAIN);
        }

        byte[] hash = new byte[0];

        try
        {
            hash = factory.generateSecret(spec).getEncoded();
        } catch (InvalidKeySpecException ikse)
        {
            LOGGER.error("Couldn't generate hash. - ", ikse);
            System.out.println(GlobalConstrains.FUNC_DOESNT_WORK_TRY_AGAIN);
        }

        return Base64.getEncoder().encodeToString(hash);
    }
}
