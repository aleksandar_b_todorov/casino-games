package com.isoft.casinogames.streamio.impl;

import java.util.HashMap;
import java.util.Scanner;

import com.isoft.casinogames.menu.Menu;
import com.isoft.casinogames.utils.GlobalConstrains;
import com.isoft.casinogames.utils.InputValidator;
import com.isoft.casinogames.utils.PasswordEncrypter;
import com.isoft.casinogames.streamio.api.Inputable;
import com.isoft.casinogames.utils.UserUniqueValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Asking the user to type nickname, password, email, names, age, passportNumber, money.
 * Every method works until correct data is found.
 */
public class InputScanner implements Inputable
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonStreamIO.class);
    private static final Scanner scanner = new Scanner(System.in);
    private static final HashMap<String, String> messages = Menu.getMessages();

    public InputScanner(){}

    @Override
    public String inputNickname()
    {
        String nickName = "";

        while (true)
        {
            System.out.print(messages.get("chooseNickname" + Menu.getLanguage()));
            nickName = scanner.nextLine();

            if (!InputValidator.validateLength(nickName, 3, 30))
            {
                System.out.printf(messages.get("shouldBeMoreOrLessSymbols" + Menu.getLanguage())
                        , messages.get("nickname" + Menu.getLanguage()), 3, 30).println();
                continue;
            }


            if (!InputValidator.validateLettersDigitsUnderscore(nickName))
            {
                System.out.println(messages.get("nicknameValidation" + Menu.getLanguage()));
                continue;
            }

            if (!UserUniqueValidator.isNicknameUnique(nickName))
            {
                System.out.println(messages.get("nicknameAlreadyExists" + Menu.getLanguage()));
                continue;
            }

            System.out.println(messages.get("yourNickname" + Menu.getLanguage()) + nickName + messages.get("correctAndSaved" + Menu.getLanguage()));
            break;
        }
        return nickName;
    }

    @Override
    public String inputPassword()
    {
        String password = "";

        while (true)
        {
            System.out.print(messages.get("choosePassword" + Menu.getLanguage()));
            password = scanner.nextLine();

            if (!InputValidator.validateLength(password, 4, 30))
            {
                System.out.printf(messages.get("shouldBeMoreOrLessSymbols" + Menu.getLanguage())
                        , messages.get("password" + Menu.getLanguage()), 4, 30).println();
                continue;
            }

            System.out.print(messages.get("repeatPassword" + Menu.getLanguage()));
            if (!password.equals(scanner.nextLine()))
            {
                System.out.println(messages.get("twoPasswordsDontMatch" + Menu.getLanguage()));
                continue;
            }

            password = PasswordEncrypter.encrypt(password);
            System.out.println(messages.get("password" + Menu.getLanguage())+ messages.get("correctAndSaved" + Menu.getLanguage()));
            break;
        }
        return password;
    }

    @Override
    public String inputEmail()
    {
        String email = "";

        while (true)
        {
            System.out.print(messages.get("chooseEmail" + Menu.getLanguage()));
            email = scanner.nextLine();
            if (!InputValidator.validateEmail(email)) // validates if email matches ***@***.***
            {
                System.out.println(messages.get("incorrectEmailInput" + Menu.getLanguage()));
                continue;
            }

            if (!InputValidator.validateLength(email, 6, 30))
            {
                System.out.printf(messages.get("shouldBeMoreOrLessSymbols" + Menu.getLanguage())
                        , messages.get("email" + Menu.getLanguage()), 6, 30).println();
                continue;
            }

            //checks if email is unique
            if (!UserUniqueValidator.isEmailUnique(email))
            {
                System.out.println(messages.get("emailAlreadyExists" + Menu.getLanguage()));
                continue;
            }

            System.out.println(messages.get("yourEmail" + Menu.getLanguage()) + email + messages.get("correctAndSaved" + Menu.getLanguage()));
            break;
        }
        return email;
    }

    @Override
    public String inputName(String typeName)
    {
        String userDataName = "";

        while (true)
        {
            System.out.printf(messages.get("chooseA%s" + Menu.getLanguage()), typeName);
            userDataName = scanner.nextLine();
            if (!InputValidator.validateLength(userDataName, 2, 20))
            {
                System.out.printf(messages.get("shouldBeMoreOrLessSymbols" + Menu.getLanguage()), typeName, 2, 20).println();
                continue;
            }
            if (!InputValidator.validateLettersAndHyphen(userDataName))
            {
                System.out.printf(messages.get("your%sShouldContainsOnlyLetters" + Menu.getLanguage()), typeName).println();
                continue;
            }
            System.out.printf(messages.get("your%s%sIsCorrectlyStored" + Menu.getLanguage()), typeName, userDataName).println();
            break;
        }
        return userDataName;
    }

    @Override
    public int inputAge()
    {
        int age = 0;

        while (true)
        {
            System.out.print(messages.get("typeYourAge" + Menu.getLanguage()));
            try
            {
                age = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException nfe)
            {
                LOGGER.error("User tried to input age with non-number. - ", nfe);
                System.out.println(messages.get("yourAgeShouldBeNumber" + Menu.getLanguage()));
                continue;
            }
            if (InputValidator.validateAge(age))
            {
                System.out.println(messages.get("ageFrom18To90" + Menu.getLanguage()));
                continue;
            }
            System.out.println(messages.get("yourAge" + Menu.getLanguage()) + age + messages.get("correctAndSaved" + Menu.getLanguage()));
            break;
        }
        return age;
    }

    @Override
    public String inputPassportNumber()
    {
        String passportNumber = "";

        while (true)
        {
            System.out.print(messages.get("typeYourPassportNumber" + Menu.getLanguage()));
            passportNumber = scanner.nextLine();
            if (!InputValidator.validateOnlyLettersDigitsMin6Max20(passportNumber))
            {
                System.out.println(messages.get("yourPNShouldContainsOnly" + Menu.getLanguage()));
                continue;
            }

            System.out.println(messages.get("yourPN" + Menu.getLanguage()) + passportNumber + messages.get("correctAndSaved" + Menu.getLanguage()));
            break;
        }
        return passportNumber;
    }

    @Override
    public String inputMoney(String transferType)
    {
        String userData = "";
        while (true)
        {
            userData = scanner.nextLine();
            if (!InputValidator.validateMoney(userData))
            {
                System.out.printf(messages.get("incorrectMoneyFormat" + Menu.getLanguage()), transferType).println();
                System.out.print(transferType + messages.get("money" + Menu.getLanguage()));
                continue;
            }

            if (userData.length() > 13)
            {
                System.out.printf((messages.get("your%sIsTooMuch" + Menu.getLanguage())), transferType).println();
                System.out.print(transferType + messages.get("money" + Menu.getLanguage()));
                continue;
            }

            break;
        }
        return userData;
    }
}