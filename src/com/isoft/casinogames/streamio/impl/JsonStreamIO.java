package com.isoft.casinogames.streamio.impl;

import java.io.*;
import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.isoft.casinogames.utils.GlobalConstrains;
import com.isoft.casinogames.entities.User;

import com.isoft.casinogames.streamio.api.JsonCrudable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonStreamIO implements JsonCrudable
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonStreamIO.class);
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public void create(User user)
    {
        List<User> users = (List<User>) this.readAll(GlobalConstrains.LIST_USER_TYPE, GlobalConstrains.JSON_PATH_USERS);
        users.add(user);
        try (FileWriter writer = new FileWriter(GlobalConstrains.JSON_PATH_USERS))
        {
            gson.toJson(users, writer);
        } catch (IOException ioe)
        {
            handleIOException(ioe, GlobalConstrains.IOSTREAM_FILE_BROKEN, GlobalConstrains.JSON_PATH_USERS);
        }
    }

    @Override
    public Object readAll(Type type, String path)
    {
        try (JsonReader reader = new JsonReader(new FileReader(path)))
        {
            return gson.fromJson(reader, type);
        } catch (FileNotFoundException fnfe)
        {
            handleIOException(fnfe, GlobalConstrains.FILE_NOT_FOUND, path);
        } catch (IOException ioe)
        {
            handleIOException(ioe, GlobalConstrains.IOSTREAM_FILE_BROKEN, path);
        }
        return null;
    }

    @Override
    public User readByNickname(String nickname)
    {
        List<User> users = (List<User>) this.readAll(GlobalConstrains.LIST_USER_TYPE, GlobalConstrains.JSON_PATH_USERS);
        return users.stream().filter(user -> user.getNickname().equals(nickname)).findAny().orElse(null);
    }

    @Override
    public User readByEmail(String email)
    {
        List<User> users = (List<User>) this.readAll(GlobalConstrains.LIST_USER_TYPE, GlobalConstrains.JSON_PATH_USERS);
        return users.stream().filter(user -> user.getEmail().equals(email)).findAny().orElse(null);
    }

    @Override
    public void update(User user)
    {
        List<User> users = (List<User>) this.readAll(GlobalConstrains.LIST_USER_TYPE, GlobalConstrains.JSON_PATH_USERS);
        // checks nickname and email because one of them could be already updated with new one.
        User userToEdit = users.stream().filter(u -> u.getNickname().equals(user.getNickname())).findAny().orElse(null);
        if (userToEdit == null)
            userToEdit = users.stream().filter(u -> u.getEmail().equals(user.getEmail())).findAny().orElse(null);

        //finds old user info(userToEdit) and set it to the new one(user)
        users.set(users.indexOf(userToEdit), user);

        try (FileWriter writer = new FileWriter(GlobalConstrains.JSON_PATH_USERS))
        {
            gson.toJson(users, writer);
        } catch (IOException ioe)
        {
            handleIOException(ioe, GlobalConstrains.IOSTREAM_FILE_BROKEN, GlobalConstrains.JSON_PATH_USERS);
        }
    }

    private void handleIOException(Exception ioe, String message, String path)
    {
        LOGGER.error(message, ioe);
        System.out.printf(GlobalConstrains.FUNC_DOESNT_WORK_TRY_AGAIN, path).println();
    }
}
