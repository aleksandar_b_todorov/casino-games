package com.isoft.casinogames.streamio.api;

/**
 * Inputable.java
 * Ask the user to input nickname,email,password,names,passportNumber,money,age.
 *
 * @author Aleksandar Todorov
 * @since 07-01-2019
 */
public interface Inputable
{
    String inputNickname();

    String inputEmail();

    String inputPassword();

    //works for firstName, middleName, lastName
    String inputName(String typeName);

    String inputPassportNumber();

    //works for withdraw and deposit
    String inputMoney(String transferType);

    int inputAge();
}
