package com.isoft.casinogames.streamio.api;

import com.isoft.casinogames.entities.User;
import java.lang.reflect.Type;

/**
 * JsonCrudable.java
 * insert , read and update a json file.
 *
 * @author Aleksandar Todorov
 * @author Borimir Georgiev
 * @since 07-16-2019
 */
public interface JsonCrudable
{
    //insert new User in file
    void create(User user);

    //read whole information from json file and return Object
    Object readAll(Type type, String path);

    User readByNickname(String nickname);

    User readByEmail(String email);

    void update(User user);
}
