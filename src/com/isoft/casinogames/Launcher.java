package com.isoft.casinogames;

import com.isoft.casinogames.utils.GlobalConstrains;
import org.apache.log4j.PropertyConfigurator;
import com.isoft.casinogames.menu.Menu;

public class Launcher
{
	public static void main(String args[])
	{
		PropertyConfigurator.configure(GlobalConstrains.FILE_PATH_LOG4J_PROPERTIES);
		System.out.println(GlobalConstrains.WELCOME_APP_MESSAGE);
		new Menu().start();
	}
}
