package com.isoft.casinogames.games.roulette.sound;

import com.isoft.casinogames.streamio.impl.JsonStreamIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static com.isoft.casinogames.utils.GlobalConstrains.IOSTREAM_FILE_BROKEN;

/**
 * Handles game sounds in different threads.
 */
public final class SoundHandler extends Thread
{
    private static String fileLoc;
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonStreamIO.class);

    private SoundHandler()
    {
    }

    public static void play(String path)
    {
        Thread t = new SoundHandler();
        SoundHandler.fileLoc = path;
        t.start();

        try
        {
            t.join();
        } catch (InterruptedException ie)
        {
            LOGGER.error("Thread interrupted." + ie);
        }
    }

    @Override
    public void run()
    {
        playSound(fileLoc);
    }

    private static void playSound(String fileLoc)
    {
        File soundFile = new File(fileLoc);

        AudioInputStream audioInputStream = null;
        try
        {
            audioInputStream = AudioSystem.getAudioInputStream(soundFile);
        } catch (UnsupportedAudioFileException uafe)
        {
            LOGGER.error("Audio format not supported." + uafe);
        } catch (IOException ioe)
        {
            LOGGER.error(IOSTREAM_FILE_BROKEN, ioe);
        } catch (IllegalArgumentException iae)
        {
            LOGGER.error("Sound can't play.", iae);
        }

        AudioFormat audioFormat = audioInputStream.getFormat();
        SourceDataLine line = null;
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
        try
        {
            line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(audioFormat);
        } catch (LineUnavailableException lue)
        {
            LOGGER.error("Line is probably in use by another application." + lue);
        }

        line.start();

        int nBytesRead = 0;
        byte[] abData = new byte[128000];
        while (nBytesRead != -1)
        {
            try
            {
                nBytesRead = audioInputStream.read(abData, 0, abData.length);
            } catch (IOException ioe)
            {
                LOGGER.error(IOSTREAM_FILE_BROKEN, ioe);
            }

            if (nBytesRead >= 0)
            {
                int nBytesWritten = line.write(abData, 0, nBytesRead);
            }
        }
        line.drain();
        line.close();
    }
}
