package com.isoft.casinogames.games.roulette.bets;

import java.math.BigDecimal;

/**
 * @author Borimir Georgiev
 */
public class RouletteBet implements Bet
{
    private BigDecimal size;
    private BetCategory betType;
    private String betValue;
    private boolean isWinning;

    public RouletteBet(BigDecimal size, BetCategory betType, String betValue)
    {
        this.size = size;
        this.betType = betType;
        this.betValue = betValue;
        this.isWinning = false;
    }

    @Override
    public BigDecimal getWinAmount()
    {
        return new BigDecimal(betType.getOdds()).multiply(size);
    }

    @Override
    public BetCategory getBetType()
    {
        return betType;
    }

    @Override
    public String getBetValue()
    {
        return betValue;
    }

    @Override
    public void setWinning(boolean winning)
    {
        isWinning = winning;
    }

    @Override
    public boolean isWinning()
    {
        return this.isWinning;
    }

    public BigDecimal getSize()
    {
        return size;
    }

    @Override
    public String toString()
    {
        return "RouletteBet{" +
                "size=" + size +
                ", betType='" + betType + '\'' +
                ", betValue='" + betValue + '\'' +
                '}';
    }
}
