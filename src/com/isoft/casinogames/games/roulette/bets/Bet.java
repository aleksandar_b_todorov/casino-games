package com.isoft.casinogames.games.roulette.bets;

import java.math.BigDecimal;

/**
 *
 *
 * @author Borimir Georgiev
 */
public interface Bet
{
    BigDecimal getWinAmount();

    BetCategory getBetType();

    String getBetValue();

    void setWinning(boolean value);

    boolean isWinning();

    BigDecimal getSize();
}
