package com.isoft.casinogames.games.roulette.bets;

/**
 * Bets spread as categories for easier calculation of odds and avoiding switch statements or multiple conditions.
 *
 * @author Borimir Georgiev
 */
public enum BetCategory
{
    NUMBER(36),
    COLOR(2),
    EVENS(2),
    DOZEN(3);

    private int odds;

    BetCategory(int odds)
    {
        this.odds = odds;
    }

    public int getOdds()
    {
        return this.odds;
    }
}
