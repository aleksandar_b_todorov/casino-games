package com.isoft.casinogames.games.roulette.numbers;

import java.util.Random;

/**
 * Generates random number.
 *
 * @author Borimir Georgiev
 */
public class NumberGenerator
{
    private Random random = new Random();

    public Numbers getRandom()
    {
        return Numbers.values()[random.nextInt(Numbers.values().length)];
    }
}
