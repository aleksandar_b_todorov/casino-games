package com.isoft.casinogames.games.roulette.numbers;

/**
 * Enum made for the roulette numbers, containing information which provides
 * information which helps in comparing the winning number with player chosen
 * number. Also each number contains it's relevant dealer sound.
 *
 * @author Borimir Georgiev
 */
public enum Numbers
{
    // public static final Numbers ZERO = new Numbers(0, "LOSE");
    ZERO(0, "GREEN", "0", "./src/resources/sounds/roulette_sounds/106438__tim-kahn__zero.wav"),
    ONE(1, "RED", "1-12", "./src/resources/sounds/roulette_sounds/106410__tim-kahn__one-red-odd.wav"),
    TWO(2, "BLACK", "1-12", "./src/resources/sounds/roulette_sounds/106437__tim-kahn__two-black-even.wav"),
    THREE(3, "RED", "1-12", "./src/resources/sounds/roulette_sounds/106425__tim-kahn__three-red-odd.wav"),
    FOUR(4, "BLACK", "1-12", "./src/resources/sounds/roulette_sounds/106406__tim-kahn__four-black-even.wav"),
    FIVE(5, "RED", "1-12", "./src/resources/sounds/roulette_sounds/106405__tim-kahn__five-red-odd.wav"),
    SIX(6, "BLACK", "1-12", "./src/resources/sounds/roulette_sounds/106413__tim-kahn__six-black-even.wav"),
    SEVEN(7, "RED", "1-12", "./src/resources/sounds/roulette_sounds/106411__tim-kahn__seven-red-odd.wav"),
    EIGHT(8, "BLACK", "1-12", "./src/resources/sounds/roulette_sounds/106401__tim-kahn__eight-black-even.wav"),
    NINE(9, "RED", "1-12", "./src/resources/sounds/roulette_sounds/106408__tim-kahn__nine-red-odd.wav"),
    TEN(10, "BLACK", "1-12", "./src/resources/sounds/roulette_sounds/106415__tim-kahn__ten-black-even.wav"),
    ELEVEN(11, "BLACK", "1-12", "./src/resources/sounds/roulette_sounds/106403__tim-kahn__eleven-black-odd.wav"),
    TWELVE(12, "RED", "1-12", "./src/resources/sounds/roulette_sounds/106426__tim-kahn__twelve-red-even.wav"),
    THIRTEEN(13, "BLACK", "13-24", "./src/resources/sounds/roulette_sounds/106417__tim-kahn__thirteen-black-odd.wav"),
    FOURTEEN(14, "RED", "13-24", "./src/resources/sounds/roulette_sounds/106407__tim-kahn__fourteen-red-even.wav"),
    FIFTEEN(15, "BLACK", "13-24", "./src/resources/sounds/roulette_sounds/106404__tim-kahn__fifteen-black-odd.wav"),
    SIXTEEN(16, "RED", "13-24", "./src/resources/sounds/roulette_sounds/106414__tim-kahn__sixteen-red-even.wav"),
    SEVENTEEN(17, "BLACK", "13-24", "./src/resources/sounds/roulette_sounds/106412__tim-kahn__seventeen-black-odd.wav"),
    EIGHTEEN(18, "RED", "13-24", "./src/resources/sounds/roulette_sounds/106402__tim-kahn__eighteen-red-even.wav"),
    NINETEEN(19, "RED", "13-24", "./src/resources/sounds/roulette_sounds/106409__tim-kahn__nineteen-red-odd.wav"),
    TWENTY(20, "BLACK", "13-24", "./src/resources/sounds/roulette_sounds/106427__tim-kahn__twenty-black-even.wav"),
    TWENTYONE(21, "RED", "13-24", "./src/resources/sounds/roulette_sounds/106432__tim-kahn__twenty-one-red-odd.wav"),
    TWENTYTWO(22, "BLACK", "13-24", "./src/resources/sounds/roulette_sounds/106436__tim-kahn__twenty-two-black-even.wav"),
    TWENTYTHREE(23, "RED", "13-24", "./src/resources/sounds/roulette_sounds/106435__tim-kahn__twenty-three-red-odd.wav"),
    TWENTYFOUR(24, "BLACK", "13-24", "./src/resources/sounds/roulette_sounds/106430__tim-kahn__twenty-four-black-even.wav"),
    TWENTYFIVE(25, "RED", "25-36", "./src/resources/sounds/roulette_sounds/106429__tim-kahn__twenty-five-red-odd.wav"),
    TWENTYSIX(26, "BLACK", "25-36", "./src/resources/sounds/roulette_sounds/106434__tim-kahn__twenty-six-black-even.wav"),
    TWENTYSEVEN(27, "RED", "25-36", "./src/resources/sounds/roulette_sounds/106433__tim-kahn__twenty-seven-red-odd.wav"),
    TWENTYEIGHT(28, "BLACK", "25-36", "./src/resources/sounds/roulette_sounds/106428__tim-kahn__twenty-eight-black-even.wav"),
    TWENTYNINE(29, "BLACK", "25-36", "./src/resources/sounds/roulette_sounds/106431__tim-kahn__twenty-nine-black-odd.wav"),
    THIRTY(30, "RED", "25-36", "./src/resources/sounds/roulette_sounds/106421__tim-kahn__thirty-red-even.wav"),
    THIRTYONE(31, "BLACK", "25-36", "./src/resources/sounds/roulette_sounds/106420__tim-kahn__thirty-one-black-odd.wav"),
    THIRTYTWO(32, "RED", "25-36", "./src/resources/sounds/roulette_sounds/106424__tim-kahn__thirty-two-red-even.wav"),
    THIRTYTHREE(33, "BLACK", "25-36", "./src/resources/sounds/roulette_sounds/106423__tim-kahn__thirty-three-black-odd.wav"),
    THIRTYFOUR(34, "RED", "25-36", "./src/resources/sounds/roulette_sounds/106419__tim-kahn__thirty-four-red-even.wav"),
    THIRTYFIVE(35, "BLACK", "25-36", "./src/resources/sounds/roulette_sounds/106418__tim-kahn__thirty-five-black-odd.wav"),
    THIRTYSIX(36, "RED", "25-36", "./src/resources/sounds/roulette_sounds/106422__tim-kahn__thirty-six-red-even.wav");

    private int numericValue;
    private String color;
    private String dozen;
    private String soundPath;

    Numbers(int numericValue, String color, String dozen, String path)
    {
        this.numericValue = numericValue;
        this.color = color;
        this.dozen = dozen;
        this.soundPath= path;
    }

    public String getSoundPath()
    {
        return soundPath;
    }

    public int getNumericValue()
    {
        return numericValue;
    }

    public String getColor()
    {
        return color;
    }

    public String getDozen()
    {
        return dozen;
    }

    public String evenOdd()
    {
        return this.getNumericValue() % 2 == 0? "even" : "odd";
    }
}
