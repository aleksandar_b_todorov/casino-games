package com.isoft.casinogames.games.roulette.cli;

import com.isoft.casinogames.games.roulette.Game;
import com.isoft.casinogames.games.roulette.bets.Bet;
import com.isoft.casinogames.games.roulette.numbers.Numbers;
import com.isoft.casinogames.games.roulette.messages.api.Dao;
import com.isoft.casinogames.games.roulette.messages.impl.RouletteMessages;
import com.isoft.casinogames.games.roulette.sound.SoundHandler;

/**
 * @author Borimir Georgiev
 */
final class DealerUI
{
    private Dao<String> messages = new RouletteMessages();
    private Game game = Game.getInstance();

    void sayWelcome()
    {
        System.out.println(messages.get("welcome"));
    }

    void sayBetOptions()
    {
        System.out.println(messages.get("bets_please"));
        System.out.println(messages.get("bet_options"));
    }

    void sayChooseNumber()
    {
        System.out.println(messages.get("choose_number"));
    }

    void sayChooseSector()
    {
        System.out.println(messages.get("choose_sector"));
    }

    void sayChooseEvenOdd()
    {
        System.out.println(messages.get("choose_even_odd"));
    }

    void sayChooseColor()
    {
        System.out.println(messages.get("choose_color"));
    }

    void askForAmount()
    {
        System.out.println(messages.get("bet_size"));
    }

    void askForMoreBets()
    {
        System.out.println(messages.get("continue_betting"));
    }

    void noMoreBetS()
    {
        System.out.println(messages.get("roulette_rolling"));
        SoundHandler.play("./src/resources/sounds/roulette_sounds/misc/106474__tim-kahn__no-more-bets.wav");

    }

    void announceWinningNumber(Numbers number)
    {
        System.out.println(messages.get("winning_number_announce"));
        SoundHandler.play("./src/resources/sounds/roulette_sounds/misc/106416__tim-kahn__the-winning-number-is.wav");

        System.out.println(number.getNumericValue() + ", "
                + number.getColor() + ", "
                + number.evenOdd() + ".");
        System.out.println();


        SoundHandler.play(number.getSoundPath());
    }

    void announceWinningBet(Bet bet)
    {
        System.out.println("Your bet on " + bet.getBetValue() + " wins " + bet.getWinAmount() + "$.");
    }

    void announceLosingBet(Bet bet)
    {
        System.out.println("Your bet on " + bet.getBetValue() + " lost " + bet.getSize() + "$.");
    }

    void announceCurrBalance()
    {
        System.out.println(messages.get("current_balance") + game.checkUserBalance().toString() + "$.");
        System.out.println();
    }

    void sayNotEnoughFunds()
    {
        System.out.println(messages.get("not_enough_money"));
    }

    void sayBetTooBig()
    {
        System.out.println(messages.get("bet_too_big"));
    }

    void sayGoodbye()
    {
        System.out.println(messages.get("goodbye"));
    }
}
