package com.isoft.casinogames.games.roulette.cli;

import com.isoft.casinogames.games.progress_bar.Progress;
import com.isoft.casinogames.games.roulette.Game;
import com.isoft.casinogames.games.roulette.bets.BetCategory;
import com.isoft.casinogames.games.roulette.numbers.Numbers;
import com.isoft.casinogames.utils.InputValidator;

import java.math.BigDecimal;

/**
 * Responsible for user interface during the game.
 *
 * @author Borimir Georgiev
 */
public class Menu
{
    private DealerUI dealer = new DealerUI();
    private Game game = Game.getInstance();

    private String betValue = "";
    private BigDecimal betAmount;
    private BetCategory betCategory = null;

    public void on()
    {
        Progress.run();
        dealer.sayWelcome();
        startRound();
    }

    private void off()
    {
        dealer.sayGoodbye();
        game.stop();
    }

    /**
     * Checks if user has money, if true -> shows menu with bet options.
     * Each case from the switch is used to build a bet,
     * according to the user choice. Which then is passed to createBet method.
     *
     * @see Game#createBet(BigDecimal, BetCategory, String)
     */
    private void startRound()
    {
        if (!userHasMoney())
        {
            return;
        }

        dealer.sayBetOptions();

        String playerChoice = UIReader.read();

        switch (playerChoice)
        {
            case "1":
                numberBet();
                break;
            case "2":
                evenOddBet();
                break;
            case "3":
                colorBet();
                break;
            case "4":
                dozenBet();
                break;
            case "5":
                dealer.announceCurrBalance();
                startRound();
                break;
            case "6":
                off();
                return;
            default:
                System.out.println("Invalid input. Please provide a number from 1-6.");
                startRound();
                break;
        }

        dealer.askForAmount();
        betAmount = getBetAmount();

        while (betAmount.compareTo(game.checkUserBalance()) > 0)
        {
            dealer.sayBetTooBig();
            betAmount = new BigDecimal(UIReader.read());
        }

        game.reduceMoney(betAmount);
        acceptBet(betValue, betAmount, betCategory);

        dealer.askForMoreBets();
        playerChoice = UIReader.read();

        if (playerChoice.equalsIgnoreCase("y"))
        {
            startRound();
        } else if (playerChoice.equalsIgnoreCase("n"))
        {
            rouletteRoll();
        } else
        {
            System.out.println("Invalid input. Please choose and type Y/N.");

            while (!playerChoice.equalsIgnoreCase("y") && !playerChoice.equalsIgnoreCase("n"))
            {
                playerChoice = UIReader.read();
            }

            startRound();
        }
    }

    private void dozenBet()
    {
        dealer.sayChooseSector();

        betValue = UIReader.read();

        while (!betValue.equalsIgnoreCase("1-12")
                && !betValue.equalsIgnoreCase("13-24")
                && !betValue.equalsIgnoreCase("25-36"))
        {
            System.out.println("Invalid input. Valid sectors are: 1-12, 13-24, 25-36.");
            betValue = UIReader.read();
        }

        betCategory = BetCategory.DOZEN;
    }

    private void colorBet()
    {
        dealer.sayChooseColor();

        betValue = UIReader.read();

        while (!betValue.equalsIgnoreCase("red") && !betValue.equalsIgnoreCase("black"))
        {
            System.out.println("Invalid input. Please choose between red/black.");
            betValue = UIReader.read();
        }

        betCategory = BetCategory.COLOR;
    }

    private void evenOddBet()
    {
        dealer.sayChooseEvenOdd();
        betValue = UIReader.read();

        while (!betValue.equalsIgnoreCase("even") && !betValue.equalsIgnoreCase("odd"))
        {
            System.out.println("Invalid input. Please choose between even/odd.");
            betValue = UIReader.read();
        }
        betCategory = BetCategory.EVENS;
    }

    private void numberBet()
    {
        dealer.sayChooseNumber();
        betCategory = BetCategory.NUMBER;
        betValue = UIReader.read();

        while (!InputValidator.validateRouletteNumber(betValue))
        {
            System.out.println("Invalid input. Please provide a number between 0-36");
            betValue = UIReader.read();
        }
    }


    private BigDecimal getBetAmount()
    {
        String playerChoice;
        playerChoice = UIReader.read();

        while (!InputValidator.validateMoney(playerChoice))
        {
            System.out.println("Please input a correct money format.");
            playerChoice = UIReader.read();
        }

        while (Double.parseDouble(playerChoice) <= 0)
        {
            System.out.println("Invalid bet size.");
            playerChoice = UIReader.read();
        }

        return new BigDecimal(playerChoice);
    }

    private boolean userHasMoney()
    {
        if (game.checkUserBankrupt())
        {
            dealer.sayNotEnoughFunds();
            // turn off game
            return false;
        }
        return true;
    }

    private void rouletteRoll()
    {
        dealer.noMoreBetS();

        Numbers number = game.rollRoulette();
        dealer.announceWinningNumber(number);
        announceBetStatus();
    }

    private void announceBetStatus()
    {
        game.getBets().forEach(bet ->
        {

            if (bet.isWinning())
            {
                dealer.announceWinningBet(bet);
            } else
            {
                dealer.announceLosingBet(bet);
            }
        });

        clearBets();
        dealer.announceCurrBalance();
        startRound();
    }

    /**
     * Clears bets from the list of bets for the new round.
     */
    private void clearBets()
    {
        game.clearBets();
    }


    private void acceptBet(String number, BigDecimal amount, BetCategory category)
    {
        game.createBet(amount, category, number);
    }
}
