package com.isoft.casinogames.games.roulette.cli;

import java.util.Scanner;

/**
 * @author Borimir Georgiev
 */
final class UIReader
{
    private static Scanner sc = new Scanner(System.in);

    private UIReader() {}

     static String read()
    {
        return sc.nextLine();
    }
}