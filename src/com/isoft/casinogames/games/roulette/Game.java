package com.isoft.casinogames.games.roulette;

import com.isoft.casinogames.entities.User;
import com.isoft.casinogames.games.roulette.bets.Bet;
import com.isoft.casinogames.games.roulette.bets.RouletteBet;
import com.isoft.casinogames.games.roulette.numbers.Numbers;
import com.isoft.casinogames.games.roulette.bets.BetCategory;
import com.isoft.casinogames.games.roulette.numbers.NumberGenerator;
import com.isoft.casinogames.games.roulette.cli.Menu;
import com.isoft.casinogames.games.roulette.sound.SoundHandler;
import com.isoft.casinogames.streamio.impl.JsonStreamIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which is in charge for the main game logic.
 * Runs the menu, creates bets, gives random numbers, checks for
 * winning numbers etc. Working out the main game workflow.
 *
 * @author Borimir Georgiev
 */
public class Game
{
    static final Logger LOGGER = LoggerFactory.getLogger(JsonStreamIO.class);
    private boolean isRunning = false;
    private NumberGenerator numbers = new NumberGenerator();
    private List<Bet> bets = new ArrayList<>();
    private User player;

    private static Game instance = null;

    private Game()
    {
    }

    public static Game getInstance()
    {
        if (instance == null)
            instance = new Game();

        return instance;
    }

    public void start(User user)
    {
        this.isRunning = true;
        this.player = user;
        Menu menu = new Menu();
        menu.on();
    }

    public void stop()
    {
        this.isRunning = false;
    }

    /**
     * Creates bet
     *
     * @param size size of the bet
     * @param betType the type /number, dozen, even/odd, red/black/ in order to work out the odds
     * @param betValue the real value of the bet. E.g.: 23, 'red', 'odd'
     */
    public void createBet(BigDecimal size, BetCategory betType, String betValue)
    {
        Bet bet = new RouletteBet(size, betType, betValue.toLowerCase());
        bets.add(bet);
    }

    private Numbers getRandomValue()
    {
        return numbers.getRandom();
    }

    /**
     * @return winning number as enum Numbers
     */
    public Numbers rollRoulette()
    {
        Numbers winningNum;

        SoundHandler.play("./src/resources/sounds/roulette_sounds/misc/277-Roulette-Wheel.wav");

        winningNum =  getRandomValue();
        setWinners(winningNum);
        addWinnings();

        return winningNum;
    }

    private void setWinners(Numbers number)
    {
        for (Bet bet : bets)
        {
            switch (bet.getBetType())
            {
                case COLOR:
                    if (bet.getBetValue().equalsIgnoreCase(number.getColor()))
                    {
                        bet.setWinning(true);
                    }
                    break;
                case NUMBER:
                    if (Integer.parseInt(bet.getBetValue()) == (number.getNumericValue()))
                    {
                        bet.setWinning(true);
                    }
                    break;
                case DOZEN:
                    if (bet.getBetValue().equalsIgnoreCase(number.getDozen()))
                    {
                        bet.setWinning(true);
                    }
                    break;
                case EVENS:
                    if (bet.getBetValue().equalsIgnoreCase(number.evenOdd()))
                    {
                        bet.setWinning(true);
                    }
                    break;

                default:

                    break;
            }
        }
    }

    public BigDecimal checkUserBalance()
    {
        return player.getMoney();
    }

    public boolean checkUserBankrupt()
    {
        return player.getMoney().compareTo(BigDecimal.valueOf(0)) <= 0;
    }

    public List<Bet> getBets()
    {
        return bets;
    }

    private void addWinnings()
    {
        BigDecimal playerMoney = player.getMoney();

        for (Bet bet : bets)
        {
            if (bet.isWinning())
            {
                player.setMoney(playerMoney.add(bet.getWinAmount()));
            }
        }

        JsonStreamIO jsonStreamIO = new JsonStreamIO();

        jsonStreamIO.update(player);
    }

    public void reduceMoney(BigDecimal betSize)
    {
        BigDecimal money = player.getMoney();

        player.setMoney(money.subtract(betSize));

        JsonStreamIO jsonStreamIO = new JsonStreamIO();

        jsonStreamIO.update(player);
    }

    public void clearBets()
    {
        bets.clear();
    }


}
