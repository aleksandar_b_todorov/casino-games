package com.isoft.casinogames.games.roulette.messages.impl;

import com.isoft.casinogames.games.roulette.messages.api.Dao;
import com.isoft.casinogames.streamio.impl.JsonStreamIO;
import com.isoft.casinogames.utils.GlobalConstrains;

import java.util.HashMap;

/**
 * Hashmap containing all the messages which are used in the casino game.
 *
 * @author Borimir Georgiev
 */
public class RouletteMessages implements Dao<String>
{
    JsonStreamIO jsonStreamIO = new JsonStreamIO();
    HashMap<String, String> messages =
            (HashMap<String, String>) jsonStreamIO.readAll(GlobalConstrains.HASHMAP_TYPE,
            "./src/resources/roulette_messages.json");

    @Override
    public String get(String key)
    {
        return messages.get(key);
    }
}
