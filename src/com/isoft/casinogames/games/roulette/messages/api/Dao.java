package com.isoft.casinogames.games.roulette.messages.api;

/**
 * @author Borimir Georgiev
 */
public interface Dao<T>
{
    T get(T key);
}
