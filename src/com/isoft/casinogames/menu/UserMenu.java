package com.isoft.casinogames.menu;

import com.isoft.casinogames.games.roulette.Game;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class UserMenu extends Menu
{
    UserMenu(){}

    void showUserMenu()
    {
        System.out.println(messages.get("loggedUsrMenu" + language));

        final Set<String> USER_OPTIONS_TO_6 = new HashSet<>(Arrays.asList("1", "2", "3", "4", "5", "6"));
        String userData = "";
        do //read user data until user press 1,2,3,4,5 or 6 and `ENTER`
        {
            userData = scanner.nextLine();
            switch (userData)
            {
                case "1":
                    this.changeProfile();
                    break;
                case "2":
                    this.fundsMenu();
                    break;
                case "3":
                    this.gamesMenu();
                    break;
                case "4":
                    contactAdmin(user.getNickname());
                    this.showUserMenu();
                    break;
                case "5":
                    changeLanguage();
                    break;
                case "6":
                    logout();
                    break;
            }
        } while (!USER_OPTIONS_TO_6.contains(userData));
    }

    private void changeProfile()
    {
        printUserInfo();
        String userData = "";
        final Set<String> USER_OPTIONS_TO_6 = new HashSet<>(Arrays.asList("1", "2", "3", "4", "5", "6", "0"));
        do //read user data until user press 1,2,3,4,5 or 6 and `ENTER`
        {
            userData = scanner.nextLine();
            switch (userData)
            {
                case "1": // Change Nickname
                    nickname = inputScanner.inputNickname();
                    user.setNickname(nickname);
                    LOGGER.info("User with email : {} changed his nickname to : {}", user.getEmail(), nickname);
                    break;
                case "2": // Change Email
                    email = inputScanner.inputEmail();
                    user.setEmail(email);
                    LOGGER.info("User with nickname : {} changed his email to : {}", user.getNickname(), email);
                    break;
                case "3": // Change Password
                    password = inputScanner.inputPassword();
                    user.setPassword(password);
                    LOGGER.info("User with nickname : {} changed his password", user.getNickname());
                    break;
                case "4": // Change Names
                    String firstName = inputScanner.inputName(messages.get("firstName" + language));
                    user.setFirstName(firstName);
                    String middleName = inputScanner.inputName(messages.get("middleName" + language));
                    user.setMiddleName(middleName);
                    String lastName = inputScanner.inputName(messages.get("lastName" + language));
                    user.setLastName(lastName);
                    LOGGER.info("User with nickname : {} changed his names to : {} {} {}", user.getNickname(), firstName, middleName, lastName);
                    break;
                case "5": // Change Age
                    int age = inputScanner.inputAge();
                    user.setAge(age);
                    LOGGER.info("User with nickname : {} changed his age to : {}", user.getNickname(), age);
                    break;
                case "6": // Change PassportNumber
                    String passportNumber = inputScanner.inputPassportNumber();
                    user.setPassportNumber(passportNumber);
                    LOGGER.info("User with nickname : {} changed his passportNumber to {}", user.getNickname(), passportNumber);
                    break;
                case "0": // Return to previews Menu
                    this.showUserMenu();
                    break;
            }
        } while (!USER_OPTIONS_TO_6.contains(userData));
        jsonStreamIO.update(user);
        this.showUserMenu();
    }

    private void printUserInfo()
    {
        System.out.println(messages.get("dashes"));
        System.out.printf(messages.get("userDetails" + language),
                user.getNickname(), user.getEmail(), user.getFirstName(),
                user.getMiddleName(), user.getLastName(), user.getAge(), user.getPassportNumber());
        System.out.println(messages.get("chooseOption" + language));
        System.out.println(messages.get("userEditDetails" + language));
    }

    private void fundsMenu()
    {
        System.out.printf(messages.get("yourFundsAre" + language), user.getMoney()).println();
        System.out.println(messages.get("chooseOption" + language));
        System.out.println(messages.get("fundsMenu" + language));

        final Set<String> USER_OPTIONS_TO_4 = new HashSet<>(Arrays.asList("1", "2", "3", "0"));
        String userData = "";
        do // read user data until user press 1, 2 or 3 and `ENTER`
        {
            userData = scanner.nextLine();
            if (userData.equals("1")) this.depositMoney();
            if (userData.equals("2")) this.withdrawMoney();
            if (userData.equals("3")) this.fundsHistory();
            if (userData.equals("0")) this.showUserMenu();
        } while (!USER_OPTIONS_TO_4.contains(userData));
    }

    private void depositMoney()
    {
        System.out.print(messages.get("depositMoney" + language));
        String money = inputScanner.inputMoney(messages.get("deposit" + language));
        user.setMoney(new BigDecimal(money).add(user.getMoney()));
        LOGGER.info("User with nickname : {} deposited {}.", user.getNickname(), money);
        insertFundsHistory(money, messages.get("deposited" + language));
        jsonStreamIO.update(user);
        System.out.println(money + messages.get("depositedSuccessfully" + language));
        this.fundsMenu();
    }

    private void withdrawMoney()
    {
        String money = "";
        System.out.print(messages.get("withdrawMoney" + language));
        money = inputScanner.inputMoney(messages.get("withdraw" + language));
        while (new BigDecimal(money).compareTo(user.getMoney()) == 1)
        {
            System.out.println(messages.get("insufficientFunds" + language));
            System.out.print(messages.get("withdrawMoney" + language));
            money = inputScanner.inputMoney(messages.get("withdraw" + language));
        }

        user.setMoney(user.getMoney().subtract(new BigDecimal(money)));
        insertFundsHistory(money, messages.get("withdrawn" + language));
        jsonStreamIO.update(user);
        LOGGER.info("User with nickname : {} withdrawn {}.", user.getNickname(), money);
        System.out.println(money + messages.get("withdrawSuccessful" + language));
        this.fundsMenu();
    }

    private void insertFundsHistory(String money, String transaction)
    {
        List<String> tempList = user.getFundsHistory();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        LocalDateTime dateTime = LocalDateTime.now();
        tempList.add(dateTime.format(formatter) + transaction + money + "$");
        user.setFundsHistory(tempList);
    }

    private void fundsHistory()
    {
        System.out.println(messages.get("history" + language));
        if (user.getFundsHistory().isEmpty())
        {
            System.out.println(messages.get("youHaventDepositedOrWithdrawnYet" + language));
        } else
        {
            user.getFundsHistory().forEach(System.out::println);
        }
        this.press0ShowUserMenu();
    }

    //TODO implement games logic
    private void gamesMenu()
    {
        Game.getInstance().start(user);
        this.showUserMenu();
    }

    private void press0ShowUserMenu()
    {
        System.out.println(messages.get("pressZero" + language));
        while (true)
        {
            if (scanner.nextLine().equals("0"))
            {
                this.showUserMenu();
                break;
            }
        }
    }
}
