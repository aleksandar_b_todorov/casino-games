package com.isoft.casinogames.menu;

import java.math.BigDecimal;
import java.util.*;

import com.isoft.casinogames.utils.GlobalConstrains;
import com.isoft.casinogames.entities.User;
import com.isoft.casinogames.utils.InputValidator;
import com.isoft.casinogames.utils.PasswordEncrypter;
import com.isoft.casinogames.streamio.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Menu
{
    static final Logger LOGGER = LoggerFactory.getLogger(JsonStreamIO.class);
    static Scanner scanner = new Scanner(System.in);
    static User user = null;

    final InputScanner inputScanner = new InputScanner();
    static final JsonStreamIO jsonStreamIO = new JsonStreamIO();
    static final HashMap<String, String> messages = (HashMap<String, String>) jsonStreamIO.readAll(GlobalConstrains.HASHMAP_TYPE, GlobalConstrains.JSON_PATH_MESSAGES);
    String nickname = "";
    String password = "";
    String email = "";

    private static int attemptsCounter = 0;
    private static String oldValue = "";
    static List<String> userMessages = new ArrayList<>();
    static String language = "En";

    public static String getLanguage()
    {
        return language;
    }

    public static HashMap<String, String> getMessages()
    {
        return messages;
    }

    public void start()
    {
        // Add admin if users.json is empty
        if (jsonStreamIO.readAll(GlobalConstrains.LIST_USER_TYPE, GlobalConstrains.JSON_PATH_USERS) == null)
        {
            insertAdminToFile();
        }

        System.out.println(messages.get("chooseOption" + language));
        System.out.println(messages.get("startMenu" + language));

        final Set<String> USER_OPTIONS_TO_4 = new HashSet<>(Arrays.asList("1", "2", "3", "4"));
        String userData = "";
        do //read user data until user press 1,2,3 or 4 and `ENTER`
        {
            userData = scanner.nextLine();
            switch (userData)
            {
                case "1":
                    this.register();
                    break;
                case "2":
                    this.login();
                    break;
                case "3":
                    this.contactAdmin("$AnonymousUser$");
                    this.start();
                    break;
                case "4":
                    this.changeLanguage();
                    break;
            }
        } while (!USER_OPTIONS_TO_4.contains(userData));
    }

    /*
     * ask the user to enter correctly : Nickname, Email, Password, FirstName,
     * MiddleName, LastName, Age, PassportNumber
     */
    private void register()
    {
        System.out.println(messages.get("dashes"));
        System.out.println(messages.get("registerStart" + language));

        LOGGER.info("User trying to create account.");

        nickname = inputScanner.inputNickname();
        email = inputScanner.inputEmail();
        password = inputScanner.inputPassword();
        String firstName = inputScanner.inputName(messages.get("firstName" + language));
        String middleName = inputScanner.inputName(messages.get("middleName" + language));
        String lastName = inputScanner.inputName(messages.get("lastName" + language));
        int age = inputScanner.inputAge();
        String passportNumber = inputScanner.inputPassportNumber();

        user = new User(nickname, email, password, firstName, middleName, lastName, age, passportNumber,
                BigDecimal.valueOf(0), false, false);
        jsonStreamIO.create(user);
        LOGGER.info("User with nickname : {} created.", user.getNickname());
        System.out.println(messages.get("registerSucc" + language));
        this.login();
    }

    private void login()
    {
        System.out.println(messages.get("dashes"));

        String nicknameOrEmail = ""; // could be Nickname or Email
        // asks user to input Nickname or Email
        while (true)
        {
            System.out.print(messages.get("loginStart" + language));
            nicknameOrEmail = inputNicknameOrEmail();
            if (nicknameOrEmail == null) continue;
            break;
        }

        System.out.print(messages.get("typePass" + language));
        password = scanner.nextLine();

        // validates password length
        while (!InputValidator.validateLength(password, 4, 30))
        {
            System.out.printf(messages.get("shouldBeMoreOrLessSymbols" + Menu.getLanguage())
                    , messages.get("password" + Menu.getLanguage()), 4, 30).println();
            System.out.print(messages.get("typePass" + language));
            password = scanner.nextLine();
        }

        // checks if this nickname/email exists
        if (user == null)
        {
            System.out.println(messages.get("invalidUserPass" + language));
            login();
        }

        //checks if password is correct
        if (user.getPassword().equals(PasswordEncrypter.encrypt(password)))
        {
            if (!user.isBlocked()) //checks if user is blocked and if yes- log in, if no - return to login
            {
                LOGGER.info("{} logged into the app.", user.getNickname());
                this.showLoginMenu();
            } else
            {
                System.out.println(messages.get("blockedAccount" + language));
                this.login();
            }
        } else  // if user typed wrong password attemptsCounter stats to count his unsuccessful attempts to log in.
        {
            countWrongPasswordAttempts(nicknameOrEmail);
            System.out.println(messages.get("invalidUserPass" + language));
            login();
        }
    }

    void contactAdmin(String nickname)
    {
        System.out.println(messages.get("contactAdmin" + language));
        userMessages.add(nickname + " : " + scanner.nextLine());
        System.out.println(messages.get("sendMessSucc" + language));
        LOGGER.info("{} send a message to admin.", nickname);
    }

    private String inputNicknameOrEmail()
    {
        String nicknameOrEmail = "";
        nicknameOrEmail = scanner.nextLine();

        if (nicknameOrEmail.contains("@")) // Checks if user has typed email
        {
            if (!InputValidator.validateEmail(nicknameOrEmail)) // validates if email matches ***@***.***
            {
                System.out.println(messages.get("emailValidation" + language));
                return null;
            }

            // validates if correct length. InputValidator prints it's own messages.
            if (!InputValidator.validateLength(nicknameOrEmail, 6, 30))
            {
                System.out.printf(messages.get("shouldBeMoreOrLessSymbols" + Menu.getLanguage())
                        , messages.get("email" + Menu.getLanguage()), 6, 30).println();
                return null;
            }
            user = jsonStreamIO.readByEmail(nicknameOrEmail);
        } else //else user typed a nickname
        {
            if (!InputValidator.validateLettersDigitsUnderscore(nicknameOrEmail))
            {
                System.out.println(messages.get("nicknameValidation" + language));
                return null;
            }
            if (!InputValidator.validateLength(nicknameOrEmail, 3, 30))
            {
                System.out.printf(messages.get("shouldBeMoreOrLessSymbols" + Menu.getLanguage())
                        , messages.get("nickname" + Menu.getLanguage()), 3, 30).println();
                return null;
            }
            user = jsonStreamIO.readByNickname(nicknameOrEmail);
        }
        return nicknameOrEmail;
    }

    private void countWrongPasswordAttempts(String nicknameOrEmail)
    {
        if (nicknameOrEmail.equals(oldValue))
        {
            attemptsCounter++;
        } else
        {
            attemptsCounter = 1;
            oldValue = nicknameOrEmail;
        }

        if (attemptsCounter > 2)
        {
            System.out.println(messages.get("unsuccessfulAttempts" + language));
            LOGGER.info("{} is blocked after 3 unsuccessful attempts to log in.", user.getNickname());
            user.setBlocked(true);
            jsonStreamIO.update(user);
            attemptsCounter = 0;
            user = null;
            this.start();
        }
    }

    void changeLanguage()
    {
        System.out.println(messages.get("chooseOption" + language));
        System.out.println(messages.get("changeLanguage" + language));

        final Set<String> USER_OPTIONS_TO_3 = new HashSet<>(Arrays.asList("1", "2", "3"));
        String userData = "";
        do //read user data until user press 1,2 or 3 and `ENTER`
        {
            userData = scanner.nextLine();
            switch (userData)
            {
                case "1":
                    language = "En";
                    redirectAfterLanguageChange();
                    break;
                case "2":
                    language = "Ru";
                    redirectAfterLanguageChange();
                    break;
                case "3":
                    language = "Bg";
                    redirectAfterLanguageChange();
                    break;
            }
        } while (!USER_OPTIONS_TO_3.contains(userData));
    }

    private void redirectAfterLanguageChange()
    {
        if(user == null) this.start();
        else this.showLoginMenu();
    }

    private void showLoginMenu()
    {
        System.out.println(messages.get("dashes"));
        System.out.printf(messages.get("youAreLoggedIn" + language), user.getNickname()).println();
        System.out.println(messages.get("chooseOption" + language));

        if (user.isAdmin()) new AdminMenu().showAdminMenu();
        else new UserMenu().showUserMenu();
    }

    void logout()
    {
        LOGGER.info("{} logged out.", user.getNickname());
        user = null;
        start();
    }

    private void insertAdminToFile()
    {
        String pass = PasswordEncrypter.encrypt("admin");
        user = new User("admin", "admin@admins.com",
                pass, "Nqkoi", "Si", "Razbirach",
                45, "23332", BigDecimal.valueOf(0), true, false);

        jsonStreamIO.create(user);
        LOGGER.info("Admin added to empty users.json.");
    }
}