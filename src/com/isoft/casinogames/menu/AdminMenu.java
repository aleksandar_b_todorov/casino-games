package com.isoft.casinogames.menu;

import com.isoft.casinogames.utils.GlobalConstrains;
import com.isoft.casinogames.entities.User;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class AdminMenu extends Menu
{
    AdminMenu(){}

    void showAdminMenu()
    {
        System.out.println(messages.get("adminMenu" + language));

        final Set<String> USER_OPTIONS_TO_8 = new HashSet<>(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8"));
        String userData = "";
        do //read user data until user press 1,2,3,4,5,6,7 or 8 and `ENTER`
        {
            userData = scanner.nextLine();
            switch (userData)
            {
                case "1":
                    this.showUsersInfo();
                    break;
                case "2":
                    this.showBlockedUsers();
                    break;
                case "3":
                    this.toggleUser("blocked", true);
                    break;
                case "4":
                    this.toggleUser("unblocked", false);
                    break;
                case "5":
                    this.checkStatusGames();
                    break;
                case "6":
                    this.checkMessages();
                    break;
                case "7":
                    changeLanguage();
                    break;
                case "8":
                    logout();
                    break;
            }
        } while (!USER_OPTIONS_TO_8.contains(userData));
    }

    private void toggleUser(String toggle, boolean isBlocked)
    {
        System.out.print(messages.get(toggle + "Usr" + language));
        String nickname = scanner.nextLine();
        User userToToggle = jsonStreamIO.readByNickname(nickname);

        if (userToToggle == null)
        {
            System.out.println(messages.get("nameDoesntExist" + language));
            toggleUser(toggle, isBlocked);
        }
        /*
        //you can't un/block user which is un/blocked
         */
        else if (isBlocked != userToToggle.isBlocked())
        {
            userToToggle.setBlocked(isBlocked);
            jsonStreamIO.update(userToToggle);
            LOGGER.info("Admin {} user with nickname : {}", toggle, userToToggle.getNickname());
            System.out.printf(messages.get("userIsBlocked" + language),
                    userToToggle.getNickname(), messages.get(toggle + language)).println();
        } else
        {
            System.out.println(messages.get("user" + toggle + language));
        }

        System.out.print(messages.get(toggle + "More" + language));
        askToggleMoreOrNo(toggle, isBlocked);
    }

    private void askToggleMoreOrNo(String toggle, boolean isBlocked)
    {
        String userAnswer = "";
        do
        {
            userAnswer = scanner.nextLine();
            switch (userAnswer)
            {
                case "1":
                    toggleUser(toggle, isBlocked);
                    break;
                case "2":
                    this.showAdminMenu();
                    break;
                default:
                    System.out.print(messages.get("yesNoPrompt" + language));
                    break;
            }
        }
        while (!(userAnswer.equals("1") || userAnswer.equals("2")));
    }

    private void showUsersInfo()
    {
        List<User> users = (List<User>) jsonStreamIO.readAll(GlobalConstrains.LIST_USER_TYPE, GlobalConstrains.JSON_PATH_USERS);
        users.forEach(u -> System.out.println(u.toString()));
        press0ShowAdminMenu();
    }

    private void showBlockedUsers()
    {
        List<User> users = (List<User>) jsonStreamIO.readAll(GlobalConstrains.LIST_USER_TYPE, GlobalConstrains.JSON_PATH_USERS);
        users.stream().filter(User::isBlocked).forEach(System.out::println);
        press0ShowAdminMenu();
    }

    //TODO implement logic
    private void checkStatusGames()
    {
        System.out.println("Games coming SOON.");
        this.press0ShowAdminMenu();
    }

    private void checkMessages()
    {
        if (userMessages.isEmpty())
        {
            System.out.println(messages.get("dashes"));
            System.out.println(messages.get("userMessagesListEmpty" + language));
        } else
        {
            System.out.println(messages.get("dashes"));
            userMessages.forEach(System.out::println);
        }

        System.out.println(messages.get("dashes"));
        System.out.println(messages.get("clearListOrGoBack" + language));
        String adminData = "";
        do //read admin data until admin press 1 or 2 and `ENTER`
        {
            adminData = scanner.nextLine();
            if (adminData.equals("1"))
            {
                userMessages.clear();
                System.out.println(messages.get("userMessagesCleared" + language));
                this.showAdminMenu();
            }
            if (adminData.equals("2")) this.showAdminMenu();
        } while (!(adminData.equals("1") || adminData.equals("2")));
    }

    private void press0ShowAdminMenu()
    {
        System.out.println(messages.get("pressZero" + language));
        while (true)
        {
            if (scanner.nextLine().equals("0"))
            {
                this.showAdminMenu();
                break;
            }
        }
    }
}
