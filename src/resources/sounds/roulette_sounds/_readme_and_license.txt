Sound pack downloaded from Freesound
----------------------------------------

This pack of sounds contains sounds by the following user:
 - tim.kahn ( https://freesound.org/people/tim.kahn/ )

You can find this pack online at: https://freesound.org/people/tim.kahn/packs/6755/

License details
---------------

Attribution: http://creativecommons.org/licenses/by/3.0/


Sounds in this pack
-------------------

  * 106474__Corsica_S__no_more_bets.wav
    * url: https://freesound.org/s/106474/
    * license: Attribution
  * 106438__Corsica_S__zero.wav
    * url: https://freesound.org/s/106438/
    * license: Attribution
  * 106437__Corsica_S__two_black_even.wav
    * url: https://freesound.org/s/106437/
    * license: Attribution
  * 106436__Corsica_S__twenty_two_black_even.wav
    * url: https://freesound.org/s/106436/
    * license: Attribution
  * 106435__Corsica_S__twenty_three_red_odd.wav
    * url: https://freesound.org/s/106435/
    * license: Attribution
  * 106434__Corsica_S__twenty_six_black_even.wav
    * url: https://freesound.org/s/106434/
    * license: Attribution
  * 106433__Corsica_S__twenty_seven_red_odd.wav
    * url: https://freesound.org/s/106433/
    * license: Attribution
  * 106432__Corsica_S__twenty_one_red_odd.wav
    * url: https://freesound.org/s/106432/
    * license: Attribution
  * 106431__Corsica_S__twenty_nine_black_odd.wav
    * url: https://freesound.org/s/106431/
    * license: Attribution
  * 106430__Corsica_S__twenty_four_black_even.wav
    * url: https://freesound.org/s/106430/
    * license: Attribution
  * 106429__Corsica_S__twenty_five_red_odd.wav
    * url: https://freesound.org/s/106429/
    * license: Attribution
  * 106428__Corsica_S__twenty_eight_black_even.wav
    * url: https://freesound.org/s/106428/
    * license: Attribution
  * 106427__Corsica_S__twenty_black_even.wav
    * url: https://freesound.org/s/106427/
    * license: Attribution
  * 106426__Corsica_S__twelve_red_even.wav
    * url: https://freesound.org/s/106426/
    * license: Attribution
  * 106425__Corsica_S__three_red_odd.wav
    * url: https://freesound.org/s/106425/
    * license: Attribution
  * 106424__Corsica_S__thirty_two_red_even.wav
    * url: https://freesound.org/s/106424/
    * license: Attribution
  * 106423__Corsica_S__thirty_three_black_odd.wav
    * url: https://freesound.org/s/106423/
    * license: Attribution
  * 106422__Corsica_S__thirty_six_red_even.wav
    * url: https://freesound.org/s/106422/
    * license: Attribution
  * 106421__Corsica_S__thirty_red_even.wav
    * url: https://freesound.org/s/106421/
    * license: Attribution
  * 106420__Corsica_S__thirty_one_black_odd.wav
    * url: https://freesound.org/s/106420/
    * license: Attribution
  * 106418__Corsica_S__thirty_five_black_odd.wav
    * url: https://freesound.org/s/106418/
    * license: Attribution
  * 106419__Corsica_S__thirty_four_red_even.wav
    * url: https://freesound.org/s/106419/
    * license: Attribution
  * 106417__Corsica_S__thirteen_black_odd.wav
    * url: https://freesound.org/s/106417/
    * license: Attribution
  * 106416__Corsica_S__the_winning_number_is.wav
    * url: https://freesound.org/s/106416/
    * license: Attribution
  * 106415__Corsica_S__ten_black_even.wav
    * url: https://freesound.org/s/106415/
    * license: Attribution
  * 106414__Corsica_S__sixteen_red_even.wav
    * url: https://freesound.org/s/106414/
    * license: Attribution
  * 106413__Corsica_S__six_black_even.wav
    * url: https://freesound.org/s/106413/
    * license: Attribution
  * 106412__Corsica_S__seventeen_black_odd.wav
    * url: https://freesound.org/s/106412/
    * license: Attribution
  * 106411__Corsica_S__seven_red_odd.wav
    * url: https://freesound.org/s/106411/
    * license: Attribution
  * 106410__Corsica_S__one_red_odd.wav
    * url: https://freesound.org/s/106410/
    * license: Attribution
  * 106409__Corsica_S__nineteen_red_odd.wav
    * url: https://freesound.org/s/106409/
    * license: Attribution
  * 106408__Corsica_S__nine_red_odd.wav
    * url: https://freesound.org/s/106408/
    * license: Attribution
  * 106407__Corsica_S__fourteen_red_even.wav
    * url: https://freesound.org/s/106407/
    * license: Attribution
  * 106406__Corsica_S__four_black_even.wav
    * url: https://freesound.org/s/106406/
    * license: Attribution
  * 106405__Corsica_S__five_red_odd.wav
    * url: https://freesound.org/s/106405/
    * license: Attribution
  * 106404__Corsica_S__fifteen_black_odd.wav
    * url: https://freesound.org/s/106404/
    * license: Attribution
  * 106403__Corsica_S__eleven_black_odd.wav
    * url: https://freesound.org/s/106403/
    * license: Attribution
  * 106402__Corsica_S__eighteen_red_even.wav
    * url: https://freesound.org/s/106402/
    * license: Attribution
  * 106401__Corsica_S__eight_black_even.wav
    * url: https://freesound.org/s/106401/
    * license: Attribution


